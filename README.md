# PDO_SQLSERVER

## Introduction

Etapes à suivre pour l'utilsation de Base SQLSERVER pour PHP 7.x

## Installation

- Télécharger les drivers PDO :   https://docs.microsoft.com/fr-fr/sql/connect/php/download-drivers-php-sql-server?view=sql-server-ver15
- Télécharger les drivers ODBC :  https://docs.microsoft.com/fr-fr/sql/connect/odbc/download-odbc-driver-for-sql-server?view=sql-server-ver15

_________

Config SYMFONY :
    + .env :   
       - DATABASE_URL=sqlserv://db_user:db_password@db_host:db_port/db_name

    + config/packages/doctrine.yaml :
       - doctrine:
               dbal:
                    driver:  sqlsrv
                    charset: UTF-8
        