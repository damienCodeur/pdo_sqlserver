<?php
    
    // A TESTER : https://www.kinamo.fr/fr/support/faq/connexion-avec-microsoft-sql-serveur-a-partir-de-php-sur-debian-via-freetds
    
    echo ('<a href="./phpinfo.php" target="_blank" style="font-size: 18px; font-weight: 800;">PHPINFO()</a>');
    
    /*****************/ 
    // CONFIGURATION
    /*****************/
    
    $server = "localhost\\SQLExpress";
    $database = "Test";
    $user = "sa";
    $password = "root";

    $table = "dbo.Table_1";
    $col1 = "lastname";
    $col2 = "forname";

    /*****************/ 
    // CODE
    /*****************/


    try {
        $cnx = new PDO("sqlsrv:Server=$server;Database=$database", "$user", "$password");
    }
    catch(PDOException $e) {
        die("Error connecting to SQL Server: " . $e->getMessage());
    }

    echo "<p>Connected to SQL Server</p>\n";

    $query = "SELECT * FROM ". $table;  

    foreach ($cnx->query($query) as $row) {
        print $row[$col1] ."\t";
        print $row[$col2] ."\t";
        print '<br>';
    }


